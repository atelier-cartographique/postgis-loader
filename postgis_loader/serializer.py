#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
from json import loads, dumps

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import connections
from django.http import HttpResponseForbidden, Http404
from django.contrib.gis.db.models.functions import AsGeoJSON
from django.apps import apps
from django.contrib.gis.geos import GEOSGeometry


from .models import get_layer

GEOM_NOT_NULL_SETTING = getattr(settings, "GEOM_NOT_NULL", True)
MAX_DECIMAL_DIGITS = getattr(settings, "MAX_DECIMAL_DIGITS", 2)

GEOJSON_QUERY = """
SELECT row_to_json(fc)
  FROM (
    SELECT
      'FeatureCollection' AS type,
      array_to_json(array_agg(f)) AS features
    FROM (
        SELECT
          'Feature' AS type,
          ST_AsGeoJSON(ST_Transform(sd.{geometry_column}, 31370), {max_decimal_digits})::json AS geometry,
          {pk_column} as id,
          row_to_json((
            SELECT prop FROM (SELECT {field_names}) AS prop
           )) AS properties
        FROM "{schema}"."{table}" AS sd
        {where_clause}
    ) AS f
  ) AS fc;
"""

BOUNDINGBOX_CLAUSE = """
sd.{geometry_column} && ST_Transform(ST_MakeEnvelope(%s, %s, %s, %s, 31370), ST_SRID(sd.{geometry_column}))
"""

WITH_GEOMETRY_CLAUSE = """
sd.{geometry_column} IS NOT NULL
"""


def get_query(schema, table, query_template, **kwargs):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append('"{}"'.format(field.column))
    pk_field = model._meta.pk.column

    # Where clause to restrict SELECT to bbox
    clauses = []
    bbox = kwargs.get("bbox")
    if bbox:
        clauses.append(
            BOUNDINGBOX_CLAUSE.format(geometry_column=geometry_field, bbox=bbox)
        )
    if GEOM_NOT_NULL_SETTING:
        clauses.append(WITH_GEOMETRY_CLAUSE.format(geometry_column=geometry_field))

    where_clause = ""
    if len(clauses) > 0:
        where_clause = "WHERE {}".format(
            " AND ".join(map(lambda x: "({})".format(x), clauses))
        )

    return query_template.format(
        pk_column=pk_field,
        schema=schema,
        table=table,
        max_decimal_digits=MAX_DECIMAL_DIGITS,
        geometry_column=geometry_field,
        field_names=", ".join(fields),
        where_clause=where_clause,
        **kwargs,
    )


NUMBER = "number"
STRING = "string"
BOOLEAN = "boolean"
DATE = "date"
DATETIME = "datetime"

# fields found in django/db/models/fields/__init__.py
MAPPED_TYPES = {
    "AutoField": NUMBER,
    "BigAutoField": NUMBER,
    "BigIntegerField": NUMBER,
    "BinaryField": STRING,
    "BooleanField": BOOLEAN,
    "CharField": STRING,
    # 'CommaSeparatedIntegerField',
    "DateField": DATE,
    "DateTimeField": DATETIME,
    # We needed to get numbers ipo String for decimals and therefore use a specific JSONEncoder (see encoder.py) (nw)
    "DecimalField": NUMBER,
    "DurationField": NUMBER,
    "EmailField": STRING,
    "FilePathField": STRING,
    "FloatField": NUMBER,
    "GenericIPAddressField": STRING,
    "IPAddressField": STRING,
    "IntegerField": NUMBER,
    "NullBooleanField": BOOLEAN,
    "PositiveIntegerField": NUMBER,
    "PositiveSmallIntegerField": NUMBER,
    "SlugField": STRING,
    "SmallIntegerField": NUMBER,
    "TextField": STRING,
    "TimeField": NUMBER,
    "URLField": STRING,
    "UUIDField": STRING,
}


def _with_metadata(schema, table, collection):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []

    for field in model._meta.get_fields():
        field_name = field.column
        if field_name != geometry_field:
            field_type = field.get_internal_type()
            try:
                fields.append([field_name, MAPPED_TYPES[field_type]])
                # print('>> {}: <{}>'.format(field_name,
                #                            MAPPED_TYPES[field_type]))
            except KeyError:
                fields.append([field_name, STRING])
                # print('Failed to map "{}" to a useful type'.format(field_type))

    collection["fields"] = fields
    return collection


def get_geojson(schema, table, **kwargs):
    query = get_query(schema, table, GEOJSON_QUERY, **kwargs)

    # print('== get_geojson ({}) ========================================'.format('bbox' in kwargs))
    # print(query)
    # print('==============================================================')

    with connections[schema].cursor() as cursor:
        bbox = kwargs.get("bbox")
        if bbox:
            cursor.execute(query, [bbox[0], bbox[1], bbox[2], bbox[3]])
        else:
            cursor.execute(query)

        row = cursor.fetchone()

    feature_collection = row[0]
    if feature_collection["features"] is None:  # can happen with a bbox
        feature_collection["features"] = []

    return _with_metadata(schema, table, feature_collection)


def get_model(schema, table):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    return model


def get_stream_metadata(schema, table):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    return _with_metadata(schema, table, {"count": model.objects.count()})


def validate_sort(sort):
    if sort is None:
        return None
    try:
        type(sort.get("column")) == int
        type(sort.get("columnName")) == str
        d = sort.get("direction").lower()
        has_dir = d == "asc" or d == "desc"
        assert has_dir
        return sort
    except Exception as ex:
        raise ValidationError("sort not well formed [{}]".format(ex))


# Get the column name corresponding to a "attname", because filters only give us the column name, and we need the good term to compare with.
def name_mapper(fields):
    def mapper(col_name):
        for field in fields:
            if field.column == col_name:
                return field.get_attname()
        raise Exception("column name not found")

    return mapper


def get_stream_data(
    schema, table, offset, limit, sort=None, filters=None, geometry=None
):
    sort = validate_sort(sort)
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append(["{}".format(field.get_attname()), "{}".format(field.column)])

    # queryset = model.objects.values_list(*fields)
    queryset = model.objects.get_queryset().annotate(geojson=AsGeoJSON(geometry_field))

    nm = name_mapper(model._meta.get_fields())

    if filters:
        for filter in filters:
            tag = filter["tag"]

            if tag == "string":
                queryset = filter_string(queryset, filter, nm)
            if tag == "number":
                queryset = filter_number(queryset, filter, nm)
            if tag == "date":
                queryset = filter_date(queryset, filter, nm)
            if tag == "datetime":
                queryset = filter_datetime(queryset, filter, nm)

    if sort:
        direction = "-" if sort.get("direction").lower() == "desc" else ""
        queryset = queryset.order_by(direction + nm(sort.get("columnName")))
    else:
        pk_field = model._meta.pk.column.lower()
        queryset = queryset.order_by(pk_field)

    if geometry:
        geom = GEOSGeometry(dumps(geometry))
        kw = {"{}__intersects".format(geometry_field): geom}
        queryset = queryset.filter(**kw)

    if GEOM_NOT_NULL_SETTING:
        kw = {"{}__isnull".format(geometry_field): False}
        queryset = queryset.filter(**kw)

    total_count = queryset.all().count()
    # data = tuple(queryset[offset:offset + limit])
    slice = queryset[offset : offset + limit]
    features = []
    for obj in slice:
        id = obj.pk
        properties = {f[1]: getattr(obj, f[0]) for f in fields}
        features.append(
            dict(
                type="Feature",
                id=id,
                properties=properties,
                geometry=loads(obj.geojson),
            )
        )

    data = tuple(features)

    return total_count, data


# TODO (if used): to adapt because we now use field.column in place of field.get_attname() in _with_metadata()
# def get_stream_feature_geojson(schema, table, feature_id):
#     model, geometry_field, _geometry_field_type = get_layer(schema, table)
#     pk_field = model._meta.pk.column

#     return serialize('geojson', model.objects.filter(**{pk_field: feature_id}), geometry_field=geometry_field, srid=31370)


def filter_string(data, filter, mapper):
    pattern = filter["pattern"]
    att_name = mapper(filter["columnName"])

    return data.filter(**{att_name + "__icontains": pattern})


def filter_number(data, filter, mapper):
    value = filter["value"]
    operator = filter["op"]
    att_name = mapper(filter["columnName"])

    if operator == "eq":
        return data.filter(**{att_name: value})
    if operator == "gt":
        return data.filter(**{att_name + "__gt": value})
    if operator == "lt":
        return data.filter(**{att_name + "__lt": value})

    return data


def filter_date(data, filter, name_mapper):
    filter["value"] = datetime.datetime.strptime(filter["date"], "%Y-%m-%d")

    return filter_number(data, filter, name_mapper)


def filter_datetime(data, filter, name_mapper):
    filter["value"] = datetime.datetime.strptime(
        filter["datetime"], "%Y-%m-%d %H:%M:%S"
    )

    return filter_number(data, filter, name_mapper)


def get_csv_data(schema, table, layer_info_id, sort=None, filters=None):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)

    # Check authorization for export
    try:
        if not LayerInfo.objects.get(id=layer_info_id).exportable:
            return HttpResponseForbidden()
    except LayerInfo.DoesNotExist:
        raise Http404

    fields = []
    python_fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append("{}".format(field.column))
            python_fields.append("{}".format(field.get_attname()))

    data = model.objects.values_list(*python_fields)

    nm = name_mapper(model._meta.get_fields())

    if filters:
        for filter in filters:
            tag = filter["tag"]

            if tag == "string":
                data = filter_string(data, filter, nm)
            if tag == "number":
                data = filter_number(data, filter, nm)
            if tag == "date":
                data = filter_date(data, filter, nm)
            if tag == "datetime":
                data = filter_datetime(data, filter, nm)

    if sort:
        direction = "-" if sort.get("direction").lower() == "desc" else ""
        data = data.order_by(direction + nm(sort.get("column")))
    else:
        pk_field = model._meta.pk.column.lower()
        data = data.order_by(pk_field)

    # Build CSV
    csv_data = ""

    for field in fields:
        csv_data += field + ";"
    csv_data += "\n"

    for item in data:
        csv_row = ""
        for attribute in item:
            csv_row += str(attribute) + ";"
        csv_data += csv_row + "\n"

    return csv_data


def alias_name(column, lang):
    Alias = apps.get_model(app_label="api", model_name="Alias")
    try:
        a = Alias.objects.get(select=column)
        return a.replace.get(lang)
    except Alias.DoesNotExist:
        return column


def filter_queryset(model, config):
    queryset = model.objects.all()
    filters = config.filters
    if filters:
        nm = name_mapper(model._meta.get_fields())
        for filter in filters:
            tag = filter["tag"]

            if tag == "string":
                return filter_string(queryset, filter, nm)
            if tag == "number":
                return filter_number(queryset, filter, nm)
            if tag == "date":
                return filter_date(queryset, filter, nm)
            if tag == "datetime":
                return filter_datetime(queryset, filter, nm)
    else:
        return queryset


def write_csv_file(schema, table, config, lang, file, geom=None):
    from csv import writer

    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = [model._meta.get_field(f) for f in config.fields]
    field_names = []
    python_fields = []
    for field in fields:
        field_names.append(alias_name(field.column, lang))
        python_fields.append(field.get_attname())

    if geom is not None:
        kwargs = {
            f"{geometry_field}__intersects": geom,
        }
        queryset = model.objects.filter(**kwargs)
    else:
        queryset = filter_queryset(model, config)

    data = queryset.values_list(*python_fields)

    # to force Excel to use utf8 encoding, bom:'ufeff'
    file.write("\uFEFF")
    csv_writer = writer(file)
    csv_writer.writerow(field_names)
    csv_writer.writerows(data)


def write_xlsx_file(schema, table, config, lang, workbook, geom=None):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = [model._meta.get_field(f) for f in config.fields]
    field_names = []
    python_fields = []
    for field in fields:
        field_names.append(alias_name(field.column, lang))
        python_fields.append(field.get_attname())

    if geom is not None:
        kwargs = {
            f"{geometry_field}__intersects": geom,
        }
        queryset = model.objects.filter(**kwargs)
    else:
        queryset = filter_queryset(model, config)

    data = queryset.values_list(*python_fields)
    worksheet = workbook.add_worksheet()
    worksheet.write_row(0, 0, field_names)
    for index, entry in enumerate(data):
        worksheet.write_row(index + 1, 0, entry)
