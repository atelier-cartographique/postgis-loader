#  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from collections import OrderedDict
from math import ceil
from django.contrib.auth.decorators import login_required
from django.db import connections
from django.shortcuts import render
from django.core.paginator import Paginator
from django.db import models as django_models

from api.models import (
    MetaData,
)
from postgis_loader.introspection import DatabaseIntrospection
from ..models import NoPKError, get_field_type
from postgis_loader import models
import re


def check_layer(schema, table_name):
    connection = connections[schema]
    introspection = DatabaseIntrospection(connection)

    def table2model(table_name):
        return re.sub(r"[^a-zA-Z0-9]", "", table_name.title())

    with connection.cursor() as cursor:
        known_models = []
        try:
            relations = introspection.get_relations(cursor, table_name, schema)
            introspection.get_constraints(cursor, table_name, schema)
            primary_key_column = introspection.get_primary_key_column(
                cursor, table_name, schema
            )

            table_description = introspection.get_table_description(
                cursor, table_name, schema
            )
        except Exception as e:
            raise e

        known_models.append(table2model(table_name))

        column_names = [row[0] for row in table_description]
        if None == primary_key_column and not "id" in column_names:
            if "gid" in column_names:
                gid_is_id = True
            else:
                raise NoPKError()

        for row in table_description:
            column_name = row[0]
            is_relation = column_name in relations
            if is_relation:
                rel_to = (
                    "self"
                    if relations[column_name][1] == table_name
                    else table2model(relations[column_name][1])
                )
                if rel_to in known_models:
                    field_type = "ForeignKey(%s" % rel_to
                else:
                    field_type = "ForeignKey('%s'" % rel_to
            else:
                # Calling `get_field_type` to get the field type string and any
                # additional parameters and notes.
                field_type, field_params, is_geometry = get_field_type(
                    connection, table_name, row
                )

            if row[6]:  # If it's NULL...
                if field_type == "BooleanField":
                    field_type = "NullBooleanField"
            try:
                getattr(models.fields, field_type)
            except AttributeError:
                getattr(django_models.fields, field_type)

        return "ok"


def uri_to_schemaname(uri):
    return uri[10:].split("/")


def check_status(metadata: MetaData):
    # print(metadata.resource_identifier)
    try:
        check_layer(*uri_to_schemaname(metadata.resource_identifier))
        return "OK"
    except Exception as e:
        return f"ERROR: {e}"


def nb_maps_from_metadata(metadata: MetaData):
    nb = 0
    for layer_info in metadata.layer_info_md.iterator():
        if layer_info.user_map_layer:
            nb = nb + layer_info.user_map_layer.count()
    return nb


HEADERS = {
    "id": {
        "fr": "Identifiant de la fiche",
        "nl": "Metadataidentificatie",
    },
    "rid": {
        "fr": "Identifiant de la ressource",
        "nl": "Bronidentificatie",
    },
    "title": {
        "fr": "Titre",
        "nl": "Title",
    },
    "status": {
        "fr": "Status de la couche",
        "nl": "Laagstatus",
    },
    "nb_maps": {
        "fr": "Nombre de cartes",
        "nl": "Aantal kaarten",
    },
    "revision": {
        "fr": "Dernière mise à jour",
        "nl": "Laatste update",
    },
}


def label_lookup(lang):
    def inner(key):
        return HEADERS[key][lang]

    return inner


@login_required
def get_layer_status(request, lang):
    headers = list(
        map(label_lookup(lang), ["rid", "title", "status", "nb_maps", "revision"])
    )

    nb_items = int(request.GET.get("nb_items", 100))
    page_number = int(request.GET.get("page", 1))

    paginator = Paginator(MetaData.objects.all().order_by("id"), nb_items)
    page_obj = paginator.get_page(page_number)

    results = []
    for md in page_obj:
        title = getattr(md.title, lang, "--")
        results.append(
            dict(
                id=md.id,
                rid=md.resource_identifier,
                title=title,
                status=check_status(md),
                nb_maps=nb_maps_from_metadata(md),
                revision=md.revision,
            )
        )

    def new_page_nb(new_nb_items):
        if page_number > 1:
            return ceil(page_obj.previous_page_number() * nb_items / new_nb_items) + 1
        return page_number

    url_20_items = f"?page={new_page_nb(20)}&nb_items=20"
    url_50_items = f"?page={new_page_nb(50)}&nb_items=50"
    url_100_items = f"?page={new_page_nb(100)}&nb_items=100"
    url_200_items = f"?page={new_page_nb(200)}&nb_items=200"

    return render(
        request,
        "postgis_loader/status.html",
        context=dict(
            headers=headers,
            page_obj=page_obj,
            results=results,
            nb_items=nb_items,
            current=page_number,
            url20=url_20_items,
            url50=url_50_items,
            url100=url_100_items,
            url200=url_200_items,
        ),
    )
