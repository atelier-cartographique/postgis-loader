#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from tempfile import TemporaryDirectory
from pathlib import PosixPath
from django.http.response import HttpResponseBadRequest
from xlsxwriter import Workbook
import io
import codecs
from json import dump, loads
from collections import namedtuple
from urllib.parse import unquote
import base64
import json
from django.core.cache import caches, InvalidCacheBackendError
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse, JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.db.models import Max, Min, Count, F

from postgis_loader.models import get_layer, normalize_col_name
from postgis_loader.encoder import JSONEncoder

from ..serializer import (
    get_geojson,
    get_stream_metadata,
    get_csv_data,
    get_stream_data,
    write_csv_file,
    write_xlsx_file,
    # get_stream_feature_geojson,
)

POSTGIS_CACHE_LAYERS = getattr(settings, "POSTGIS_CACHE_LAYERS", True)

ExportConfig = namedtuple("ExportConfig", ["fields", "filters"])


def parse_config(s):
    unquoted = unquote(s)
    decoded = base64.b64decode(unquoted).decode("utf-8")
    data = json.loads(decoded)

    return ExportConfig(
        data["fields"],
        data["filters"],
    )


def handle_request(request: HttpRequest, schema, table):
    user = request.user
    if not user.has_perm("postgis_loader", (schema, table)):
        raise PermissionDenied()

    geom_param = request.GET.get("geom")
    geom = None
    if geom_param is not None:
        try:
            geom = GEOSGeometry(geom_param)
        except Exception:
            return HttpResponseBadRequest("Could not parse geometry data")

    lang = request.GET.get("lang")
    form = request.GET.get("form")

    query_string = request.GET.get("q")
    if query_string is not None:
        config = parse_config(query_string)
    else:
        fields = list(
            map(
                lambda s: normalize_col_name(s, [], False)[0],
                request.GET.getlist("field", []),
            )
        )
        config = ExportConfig(fields, [])

    if form is not None and lang is not None:
        if len(config.fields) == 0:
            return HttpResponseBadRequest(f'"fields" specification is required')
        if form == "csv":
            return get_csv(request, schema, config, lang, table, geom=geom)
        elif form == "xlsx":
            return get_xlsx(request, schema, config, lang, table, geom=geom)
        return HttpResponseBadRequest(
            f'You asked for a form we can not provide: "{form}"'
        )

    # Extract bbox if exists
    bbox = None
    try:
        bbox = request.GET.get("bbox").split(",")
    except AttributeError:
        pass

    if not POSTGIS_CACHE_LAYERS or bbox is not None:
        return JsonResponse(get_geojson(schema, table, bbox=bbox))

    try:
        cache = caches["layers"]
        ckey = "{}.{}".format(schema, table)
        try:
            reader = cache.read(ckey)
            reader_type = type(reader)
            # print('ReaderType {} {}'.format(ckey, reader_type))
            if reader_type is io.BufferedReader:
                return FileResponse(reader, content_type="application/json")
            elif reader_type is dict:
                return JsonResponse(reader)

            response = HttpResponse(reader, content_type="application/json")
            return response

        except KeyError:
            # there's been of juggling to force diskcache
            # to return a BufferedReader from cache.read
            stream = io.BytesIO()
            writer = codecs.getwriter("utf-8")(stream)
            data = get_geojson(schema, table)
            dump(data, writer)
            stream.seek(0)
            cache.set(ckey, stream, read=True)

            return JsonResponse(data)

    except InvalidCacheBackendError:
        return HttpResponse(
            content=get_geojson(schema, table, bbox=bbox),
            content_type="application/json",
        )


@csrf_exempt
def handle_stream_request(request, schema, table):
    user = request.user
    if not user.has_perm("postgis_loader", (schema, table)):
        raise PermissionDenied()

    if request.method == "GET":
        field_interval = request.GET.get("interval")
        if field_interval is not None:
            return JsonResponse(
                encoder=JSONEncoder,
                data=get_interval(schema, table, field_interval),
            )

        field_distinct = request.GET.get("distinct")
        if field_distinct is not None:
            return JsonResponse(
                encoder=JSONEncoder,
                data=get_distinct_values(schema, table, field_distinct),
                safe=False,
            )

        return JsonResponse(get_stream_metadata(schema, table))

    elif request.method == "POST":
        params = loads(request.body.decode("utf-8"))

        total_count, data = get_stream_data(
            schema,
            table,
            params.get("offset"),
            params.get("limit"),
            params.get("sort"),
            params.get("filters"),
            params.get("geometry"),
        )

        return JsonResponse({"data": data, "totalCount": total_count})


# @csrf_exempt
# def handle_stream_feature_request(request, schema, table, feature_id):
#     user = request.user
#     if not user.has_perm('postgis_loader', (schema, table)):
#         raise PermissionDenied()

#     feature_gjs = get_stream_feature_geojson(schema, table, feature_id)
#     return HttpResponse(feature_gjs)


# the old method... still used?
def handle_csv_request(request, schema, table):
    user = request.user
    if not user.has_perm("postgis_loader", (schema, table)):
        raise PermissionDenied()

    if request.method == "GET":
        return JsonResponse(get_stream_metadata(schema, table))

    elif request.method == "POST":
        params = loads(request.body.decode("utf-8"))

        data = get_csv_data(
            schema,
            table,
            params.get("layerInfo"),
            params.get("sort"),
            params.get("filters"),
        )

        filename = "{}-{}.csv".format(schema, table)
        response = HttpResponse(data, content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="{}"'.format(filename)
        return response


def get_xlsx(request, schema, config, lang, table, geom=None):
    user = request.user
    if not user.has_perm("postgis_loader", (schema, table)):
        raise PermissionDenied()

    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        filename = f"{schema}-{table}.xlsx"
        filepath = root.joinpath(filename)

        with Workbook(filepath.as_posix()) as workbook:
            write_xlsx_file(schema, table, config, lang, workbook, geom)
        return FileResponse(filepath.open("rb"), as_attachment=True, filename=filename)


def get_csv(request, schema, config, lang, table, geom=None):
    user = request.user
    if not user.has_perm("postgis_loader", (schema, table)):
        raise PermissionDenied()

    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        filename = f"{schema}-{table}.csv"
        filepath = root.joinpath(filename)
        with filepath.open("w", encoding="utf8") as file:
            write_csv_file(schema, table, config, lang, file, geom)
        return FileResponse(filepath.open("rb"), as_attachment=True, filename=filename)


def get_interval(schema, table, field):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    return model.objects.all().aggregate(min=Min(field), max=Max(field))


def find_field(meta, column_name):
    for field in meta.fields:
        if field.column == column_name:
            return field

    raise Exception(f"Field not found for column name: {column_name}")


def get_distinct_values(schema, table, column_name):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    meta = model._meta
    field = find_field(meta, column_name).name
    return list(
        model.objects.values(value=F(field))
        .exclude(value__isnull=True)
        .annotate(count=Count("value"))
    )
